# Lemuria

## About
Lemuria is a application for replaying recorded APD files as a virtual TCP device. Other programs (e.g. Acheron) can connect to this virtual device and see the recorded data as if it was being generated live.

The Asphodel communication protocol was developed by Suprock Technologies (http://www.suprocktech.com)

## License
Lemuria is licensed under the ISC license.

The ISC license is a streamlined version of the BSD license, and permits usage in both open source and propretary projects.
